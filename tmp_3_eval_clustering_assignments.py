#%%
import pandas as pd
from sklearn import metrics
import re

def evaluate_clustering(lst_true_label, lst_pred_label, encoding=None, affinity=None, eval_method=range(1,10)):
    '''Nine types of evaluation methods available, `eval_method` (list) indicates which method will be used.
    1. Rand Score (Index) and Adjusted Rand Score (Index)
    2. Mutual Information based scores (NMI score and AMI score)
    3. Homogeneity, completeness and V-measure
    4. Fowlkes-Mallows scores
    5. Silhouette Coefficient
    6. Calinski-Harabasz Index
    7. Davies-Bouldin Index
    8. Contingency Matrix
    9. Pair Confusion Matrix
    
    Note that 
     - `5. Silhouette Coefficient`, `6. Calinski-Harabasz Index` and `7. Davies-Bouldin Index` will need also encoding tensor/matrix as input. 
     - The input `affinity` is only an option for `5. Silhouette Coefficient` method
    '''
    res = {}
    # 1. Rand Score
    if 1 in eval_method:
        res["1_rand score"] = metrics.rand_score(lst_true_label, lst_pred_label)
        res["1_adjusted_rand_score"] = metrics.adjusted_rand_score(lst_true_label, lst_pred_label)
    # 2. Mutual Information based scores
    if 2 in eval_method:
        res["2_NMI score"] = metrics.normalized_mutual_info_score(lst_true_label, lst_pred_label)
        res["2_AMI score"] = metrics.adjusted_mutual_info_score(lst_true_label, lst_pred_label)
    # 3. Homogeneity, completeness and V-measure
    if 3 in eval_method:
        res["3_homogeneity_score"] = metrics.homogeneity_score(lst_true_label, lst_pred_label)
        res["3_completeness_score"] = metrics.completeness_score(lst_true_label, lst_pred_label)
        res["3_v_measure_score"] = metrics.v_measure_score(lst_true_label, lst_pred_label, beta=1.0)
    # 4. Fowlkes-Mallows scores
    if 4 in eval_method:
        res["4_fowlkes_mallows_score"] = metrics.fowlkes_mallows_score(lst_true_label, lst_pred_label)
    # 5. Silhouette Coefficient
    if 5 in eval_method and encoding is not None :
        if affinity is not None:
            res["5_silhouette_score"] = metrics.silhouette_score(encoding, lst_pred_label, metric=affinity)
        else:
            res["5_silhouette_score"] = metrics.silhouette_score(encoding, lst_pred_label)
    # 6. Calinski-Harabasz Index
    if 6 in eval_method and encoding is not None:
        res["6_calinski_harabasz_score"] = metrics.calinski_harabasz_score(encoding, lst_pred_label)
    # 7. Davies-Bouldin Index
    if 7 in eval_method and encoding is not None:
        res["7_davies_bouldin_score"] = metrics.davies_bouldin_score(encoding, lst_pred_label)
    # 8. Contingency Matrix
    if 8 in eval_method:
        res["8_Contingency Matrix"] = metrics.cluster.contingency_matrix(lst_true_label, lst_pred_label)
    # 9. Pair Confusion Matrix
    if 9 in eval_method:
        res["9_pair_confusion_matrix"] = metrics.cluster.pair_confusion_matrix(lst_true_label, lst_pred_label)
    return res

# #%% Count cat num
# path = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/balanced/"

# df_train = pd.read_csv(path + "description_train.csv",header=0,index_col=0)
# df_test = pd.read_csv(path + "description_test.csv",header=0,index_col=0)
# df1 = df_train.append(df_test,ignore_index=True)
# df1.sort_values(by='paragraph',inplace=True)
# df1.index = range(len(df1))

# print("{:=<30}".format(""))
# print("{:-^30}".format("train"))
# print(df_train.sent_cat.value_counts())
# print("{:-^30}".format("test"))
# print(df_test.sent_cat.value_counts())
# print("{:-^30}".format("all"))
# print(df1.sent_cat.value_counts())
# print("{:=<30}".format(""))


# #%%
# path = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/imba/"
# df_train = pd.read_csv(path + "description_train.csv",header=0,index_col=0)
# df_test = pd.read_csv(path + "description_test.csv",header=0,index_col=0)
# df2 = df_train.append(df_test,ignore_index=True)
# df2.sort_values(by='paragraph',inplace=True)
# df2.index = range(len(df2))

# print("{:=<30}".format(""))
# print("{:-^30}".format("train"))
# print(df_train.sent_cat.value_counts())
# print("{:-^30}".format("test"))
# print(df_test.sent_cat.value_counts())
# print("{:-^30}".format("all"))
# print(df2.sent_cat.value_counts())
# print("{:=<30}".format(""))

# # # %%
# # df1.to_csv(path + "description.csv")

##%%
all_cols = ['data_type', 'embd_method', 'preprocess_type', 'clustering', 'metric',
       'num_clusters', '1_rand score', '1_adjusted_rand_score', '2_NMI score',
       '2_AMI score', '3_homogeneity_score', '3_completeness_score',
       '3_v_measure_score', '4_fowlkes_mallows_score', '5_silhouette_score',
       '6_calinski_harabasz_score', '7_davies_bouldin_score',
       '8_Contingency Matrix', '9_pair_confusion_matrix']

lst_ind =['data_type', 'embd_method', 'preprocess_type', 'clustering', 'metric',
    'num_clusters', '1_adjusted_rand_score', '3_v_measure_score', '4_fowlkes_mallows_score']
lst_new_col_names = ['data_type', 'embd_method', 'preprocess_type', 'clustering', 'metric',
    'num_clusters', '1_adjusted_rand_score', '2_v_measure_score', '3_fowlkes_mallows_score']



#%% df cls assignment
EXP_TYPE = 'para'
BERT_TYPE = "sbert"
CAT_LVL = 'sent'
path_df_base_para = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled"
path_df_base_sent = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents"
path_df_base = path_df_base_para if EXP_TYPE == 'para' else path_df_base_sent

num_clusters = "gapstat3_clusters" # "gapstat1_clusters", "2clusters"

file_in_bala = f"C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/clustering/assignments/bala_train_{EXP_TYPE}_{BERT_TYPE}_{num_clusters}_assignment.csv"
file_in_imba = f"C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/clustering/assignments/imba_train_{EXP_TYPE}_{BERT_TYPE}_{num_clusters}_assignment.csv"

file_out_all = f"C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/clustering/eval/eval_{EXP_TYPE}_{BERT_TYPE}_{num_clusters}_{CAT_LVL}lvlcat_allExps.csv"
file_out_chosen = f"C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/clustering/eval/eval_{EXP_TYPE}_{BERT_TYPE}_{num_clusters}_{CAT_LVL}lvlcat_allExps_col_chosen.csv"



df_clsass_bala = pd.read_csv(file_in_bala, header=0, index_col=0)
cols_cls_bala = list(df_clsass_bala.columns)

df_clsass_imba = pd.read_csv(file_in_imba, header=0, index_col=0)
cols_cls_imba = list(df_clsass_imba.columns)

##%%
def find_des_file(data_type):
    if "bala_train" in data_type:
        if "rmvents" in data_type:
            return 'balanced/description_train_rmvents.csv'
        else:
            return 'balanced/description_train.csv'
    elif "imba_train" in data_type:
        if "rmvents" in data_type:
            return 'imba/description_train_rmvents.csv'
        else:
            return 'imba/description_train.csv'
    else:
        return ""

if EXP_TYPE == 'para':
    regex = "(?P<data_type>[a-z_]+(_rmvents)*)_(?P<embd_method>(sbert)|(biobert))_(?P<pre_type>[a-z_]+)_(?P<cls_method>(HAC)|(KMedoids))_(?P<affinity>[a-z]+)_cls"
elif "sent" in EXP_TYPE:
    regex = "(?P<data_type>[a-z_]+_cat0123(_rmvents)*)_(?P<embd_method>[a-z]+)_(?P<pre_type>[a-z_]+)_(?P<cls_method>(HAC)|(KMedoids))_(?P<affinity>[a-z]+)_cls"

def run_one_df(df, df_res=None):
    if df_res is None:
        df_res = pd.DataFrame()
    for i, col in enumerate(df.columns[1:]):
        mth = re.search(regex, col)
        if mth:
            mth = re.search(regex, col)
            data_type = mth.group('data_type')
            embd_method = mth.group('embd_method')
            pre_type = mth.group('pre_type')
            cls_method = mth.group('cls_method')
            affinity = mth.group('affinity')
            print(data_type,embd_method,pre_type,cls_method,affinity)
        else:
            print(f"{i+1} found nothing", col)
            continue

        des_file = find_des_file(data_type)
        path_df_file = f"{path_df_base}/{des_file}"
        df_des = pd.read_csv(path_df_file, header=0, index_col=0)
        if 'sent' in EXP_TYPE :
            lst_cat_sentlvl = df_des["sent_cat"].astype(int).to_list()
            lst_cat_paralvl = df_des['para_cat'].astype(int).to_list()
            if CAT_LVL == 'para':
                lst_truelabel = lst_cat_paralvl
            else:
                lst_truelabel = lst_cat_sentlvl
        elif EXP_TYPE == 'para':
            lst_truelabel = df_des["cat"].astype(int).to_list()

        cluster_assignment = df[col].astype(int).tolist()
        res = evaluate_clustering(lst_truelabel, cluster_assignment)
        lst_eval_methods = list(res.keys())
        res['data_type'] = data_type
        res['embd_method'] = embd_method
        res['preprocess_type'] = pre_type
        res['clustering'] = cls_method
        res['metric'] = affinity
        res['num_clusters'] = num_clusters
        df_res = df_res.append(res, ignore_index=True)
    return df_res

df_res = run_one_df(df_clsass_bala)
df_res = run_one_df(df_clsass_imba,df_res)

df_res = df_res[['data_type','embd_method','preprocess_type','clustering','metric','num_clusters', '1_adjusted_rand_score', '1_rand score', '2_AMI score', '2_NMI score',
       '3_completeness_score', '3_homogeneity_score', '3_v_measure_score',
       '4_fowlkes_mallows_score', '8_Contingency Matrix',
       '9_pair_confusion_matrix']]

df_ = df_res[lst_ind]
df_.columns = lst_new_col_names

## %%
df_res.to_csv(file_out_all)
df_.to_csv(file_out_chosen)

# %%
