# -*- coding: utf-8 -*-
#%%
import spacy
from spacy import displacy
import re
import json
import pandas as pd
import random
from pprint import pprint
import pathlib
import tqdm
##%%
def ann_to_displacy(annots):
    res = []
    for i,annot in annots.items():
        res.append({"start": int(annot["from"])-1, "end": int(annot["to"]), "label": ','.join(annot["semantic_groups"])})
    return res

def annMADE2displacy(annots):
    lst_anns = [(int(ent['offset']), int(ent['offset'])+int(ent['length']), ent['label']) for k,ent in annots.items()]
    lst_anns = sorted(lst_anns, key=lambda x:x[0])
    res = [{"start": x[0], "end": x[1], "label": x[2]} for x in lst_anns]
    return res
#%% MADE
cat = 2
path_txt = f'C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/par/{cat}/'
path_ann = f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/MADE_para_ents/{cat}/"
path_out = f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/ent_annotations/{cat}/"
if not pathlib.Path(path_out).exists():
    print(f"Making dir: {path_out}")
    pathlib.Path(path_out).mkdir(parents=True, exist_ok=True)

suffix_txt = '.txt'
suffix_ann = '_anns.json'

colors = {'DRUG':'yellow','SSLIF':'green','ADE':'blue'}
# colors = {gp:'#'+ str(hex(random.randint(0,16777215)))[2:] for gp in semtc_gps}
# pprint(colors)

files_to_it = [x for x in pathlib.Path(path_txt).iterdir() if x.suffix in [suffix_txt]]

##%%
for afile in tqdm.tqdm(files_to_it):
    # filename = "01_0009_ADDITION"
    filename = afile.stem
    with open(afile.as_posix(), 'r', encoding='utf-8') as filehdlr:
        txt = filehdlr.read()
    with open(path_ann+filename+suffix_ann, 'r', encoding='utf-8') as filehdlr:
        annots=json.load(filehdlr)

    dict_render = [{"text": txt,
                "ents": annMADE2displacy(annots),
                "title": filename}]

    options = {"colors":colors
                }

    html = displacy.render(dict_render, style="ent", manual=True, jupyter=False, page=True, options=options)
    output_path = pathlib.Path(path_out+f"{filename}.html")
    output_path.open("w", encoding="utf-8").write(html)
# %% splitted by sents
path_txt = f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sent/"
path_ann = f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/annot/"
path_out = f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/ent_annotations/"
suffix_txt = '.txt'
suffix_ann = '_anns.json'
colors = {'DRUG':'yellow','SSLIF':'green','ADE':'blue'}
# colors = {gp:'#'+ str(hex(random.randint(0,16777215)))[2:] for gp in semtc_gps}
# pprint(colors)

files_to_it = [x for x in pathlib.Path(path_txt).iterdir() if x.suffix in [suffix_txt]]

##%%
for afile in tqdm.tqdm(files_to_it):
    # filename = "01_0009_ADDITION"
    filename = afile.stem
    with open(afile.as_posix(), 'r', encoding='utf-8') as filehdlr:
        txt = filehdlr.read()
    with open(path_ann+filename+suffix_ann, 'r', encoding='utf-8') as filehdlr:
        annots=json.load(filehdlr)

    dict_render = [{"text": txt,
                "ents": annMADE2displacy(annots),
                "title": filename}]

    options = {"colors":colors
                }

    html = displacy.render(dict_render, style="ent", manual=True, jupyter=False, page=True, options=options)
    output_path = pathlib.Path(path_out+f"{filename}.html")
    output_path.open("w", encoding="utf-8").write(html)
# %%
