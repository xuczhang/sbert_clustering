# -*- coding: utf-8 -*-
#%%
import sys
from nltk import word_tokenize,bigrams,ngrams 
import sentEmbd_clsuter as sc_
import pandas as pd
import re as re_
import json
import pathlib

import texthero as preprocess
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk import word_tokenize, pos_tag

# BAD_TOKENS = [",", "(", ")", ";", "''",  "``", "'s", "-", "vs.", "v", "'", ":", ".", "--"]
# STOP_WORDS = set(stopwords.words('english'))

#%%
def extract_bigrams(words):
    return [gram[0]+' '+gram[1] for gram in bigrams(words)]

def new_patterns(cluster,pharses,similiraty,top_k):
        s , p = sc_.similarity(cluster,pharses,False,top_k,similiraty) # explain
        return s,p

def exract_relations_sent(sentence_dict,unique_patterns,sentence_with_entities,pat_score,outpath="Evaluation/Eval_NYT/data/output_relationship.txt"):
    f_output = open(outpath, "w")
    for pat in unique_patterns:
        score = pat_score[pat]
        for key,value in sentence_with_entities.items():
            text = value[0]+' '+value[1] +' ' +value[2]
            #print(text)
            if pat in text:
                f_output.write("instance: " + key[0]+'\t'+key[1]+'\tscore:'+str(score)+'\n')
                f_output.write("sentence: "+sentence_dict[key]+'\n')
                f_output.write("pattern_bef: "+sentence_with_entities[key][0]+'\n')
                f_output.write("pattern_bet: "+sentence_with_entities[key][1]+'\n')
                f_output.write("pattern_aft: "+sentence_with_entities[key][2]+'\n')
                f_output.write("\n")
    f_output.close()

def extract_relations_window(sentence_dict,unique_patterns,all_dict,pat_score, outpath="Evaluation/Eval_NYT/data/output_relationship.txt"):
    
    f_output = open(outpath, "w")
    for pat in unique_patterns:
        score = pat_score[pat]
        bi_pat = ngrams(pat.split(),2)
        for key,value in all_dict.items():
            text = value[0]+' '+value[1] +' ' +value[2]
            tokens = word_tokenize(text)    
            bgram = ngrams(text.split(), 2)
            pat_token = word_tokenize(pat)
            count = 0
            for pt_bi in bi_pat:
                
                for st_bi in bgram:
                    
                    if st_bi == pt_bi:
                        count += 1
            for pt in pat_token:
                for st in tokens:
                    if st == pt:
                        count += 0.5
            if count * score > 2:
                f_output.write("instance: " + key[0]+'\t'+key[1]+'\tscore:'+str(count*score)+'\n')
                f_output.write("sentence: "+sentence_dict[key]+'\n')
                f_output.write("pattern_bef: "+all_dict[key][0]+'\n')
                f_output.write("pattern_bet: "+all_dict[key][1]+'\n')
                f_output.write("pattern_aft: "+all_dict[key][2]+'\n')
                f_output.write("\n")
    f_output.close()

# Text Preporcessing 
def cha_dot_repl(matchobj):
    return matchobj.group(0).replace('.','')

def abbrv_pre(x, uncased=False,remove_header=False):
    x = x.replace('\n',' ')
    x = x.replace('\r',' ')
    x = x.strip()
    y = re_.sub('\\[(.*?)\\]','',x) #remove de-identified brackets 
                                    # or specify each case ?
    # y = re_.sub(':',' ',y)
    y = re_.sub(' +',' ',y)             
    y = re_.sub('^\s*[0-9][.\/)]:* ','',y) 
    y = re_.sub('_+','',y)
    y = re_.sub('-+','',y)
    # y = re_.sub('?+','',y)
    if remove_header:
        y = re_.sub("^((([A-Z])+ )*(([A-Z])+:)*)","",y) # remove heading
    if uncased:
        y = y.lower()
    y = re_.sub(' (M|m)r\.','mister',y)
    y = re_.sub(' (M|m)rs\.','missus',y)
    y = re_.sub(' (M|m)s\.','missus',y)
    y = re_.sub(' (D|d)(R|r)\.','doctor',y)
    y = re_.sub('(([a-zA-Z]\.){2,})',cha_dot_repl,y) 
    y = re_.sub(' {2,}',' ',y)  
    y = re_.sub(' ,',',',y)
    return y

def get_wordnet_pos(treebank_tag:str):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        # Default pos for lemmatize func
        return wordnet.NOUN

def lemmatize_with_pos(content:str):
    lemmatizer = WordNetLemmatizer()
    tokens = word_tokenize(content)
    # find the pos tagginf for each tokens [('What', 'WP'), ('can', 'MD'), ('I', 'PRP') ....
    pos_tokens = pos_tag(tokens) 
    # lemmatization using pos tagg   
    return " ".join([lemmatizer.lemmatize(token, get_wordnet_pos(tag)) for token, tag in pos_tokens])

def apply_one_preprocess(df_sampled, pre_type='else', target_col='paragraph'):
    ### preprocessing 1.0.5 - 1.0.6
    ### remove_stop_words - remove_stopwords
    ### do_stem - stem
    if pre_type == 'min':
        # minimum preprocess
        df_sampling_pre = df_sampled.copy()
        df_sampling_pre[target_col] = df_sampling_pre[target_col].astype(str).apply(abbrv_pre)
    elif pre_type == 'spwd':
        # De-stopwords (default) 
        df_sampling_pre = df_sampled.copy()
        df_sampling_pre[target_col] = df_sampling_pre[target_col].astype(str).apply(abbrv_pre)
        custom_pipeline = [preprocess.preprocessing.lowercase,
                            preprocess.preprocessing.remove_stop_words,
                            preprocess.preprocessing.remove_whitespace]
        df_sampling_pre[target_col] = preprocess.clean(df_sampling_pre[target_col], custom_pipeline)
    elif pre_type == 'lemma':
        # Lemmatization
        df_sampling_pre = df_sampled.copy()
        df_sampling_pre[target_col] = df_sampling_pre[target_col].astype(str).apply(abbrv_pre)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.lowercase)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].apply(lemmatize_with_pos)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.remove_whitespace)
    elif pre_type == 'stem':
        # Stemming
        '''It makes use of two NLTK stemming algorithms known as :class:`nltk.stem.SnowballStemmer`(default) and :class:`nltk.stem.PorterStemmer`. SnowballStemmer should be used when the Pandas Series contains non-English text has it has multilanguage support.
        '''
        df_sampling_pre = df_sampled.copy()
        df_sampling_pre[target_col] = df_sampling_pre[target_col].astype(str).apply(abbrv_pre)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.lowercase)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.do_stem)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.remove_whitespace)
    elif pre_type == 'spwd_stem':
        # De-stopwords (default) + stemming
        df_sampling_pre = df_sampled.copy()
        df_sampling_pre[target_col] = df_sampling_pre[target_col].astype(str).apply(abbrv_pre)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.lowercase)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.remove_stop_words)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.do_stem)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.remove_whitespace)
    elif pre_type == 'spwd_lemma':
        # De-stopwords (default) + lemmatization
        df_sampling_pre = df_sampled.copy()
        df_sampling_pre[target_col] = df_sampling_pre[target_col].astype(str).apply(abbrv_pre)
        custom_pipeline = [preprocess.preprocessing.lowercase,
                            preprocess.preprocessing.remove_stop_words]
        df_sampling_pre[target_col] = preprocess.clean(df_sampling_pre[target_col], custom_pipeline)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].apply(lemmatize_with_pos)
        df_sampling_pre[target_col] = df_sampling_pre[target_col].pipe(preprocess.preprocessing.remove_whitespace)
    else:
        return df_sampled
    return df_sampling_pre

# Misc
def find_key(dict_,v):
    if v not in dict_.values():
        return None
    for k,vv in dict_.items():
        if vv== v:
            return k
