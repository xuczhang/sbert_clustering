# -*- coding: utf-8 -*-
#%%
import sys
import pandas as pd
import re as re_
import json
import pathlib
import tqdm

import sentEmbd_clsuter as sc_
import ure_sbert


def rmv_ents_by_anns(txt:str, annots:dict):
    if not annots:
        return txt
    # Each offset is the endpoint of a segment, the end of each entity is the startpoint of a segment
    lst_seg = [(int(ent['offset']), int(ent['offset'])+int(ent['length'])) for k,ent in annots.items()]
    lst_seg = sorted(lst_seg, key=lambda x:x[0])
    lst_seg = [x for sublist in lst_seg for x in sublist]
    lst_seg_ = [0] + lst_seg + [len(txt)]

    lst_pcs = [txt[x1:x2] for x1,x2 in zip(lst_seg_[0::2],lst_seg_[1::2])]
    txt_rmv_ent = "".join(lst_pcs)
    return txt_rmv_ent

#%% ===================== balanced MADE Train ==============================================
# Original MADE
# df = pd.read_csv("../data/corrected_MADE_data_splitted_by_paragraphs/description.csv",sep=';')
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/Topix_LTBM/Exps/MADE_ba/seed_218/processed_data/data_preprocessed_spwd.csv",header=0,index_col=0)
# df['adr'] = df['adr'].map(eval)
# df['drug_names'] = df['drug_names'].map(eval)
# df['symp_names'] = df['symp_names'].map(eval)

# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/description_train.csv",sep=';', header=0,index_col=0)
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_train.csv", header=0)
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description.csv", header=0,index_col=0)

# df['adr'] = df['adr'].map(eval)
# df['drugs'] = df['drugs'].map(eval)
# df['symptoms'] = df['symptoms'].map(eval)


##%%
path_made = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sent"
path_annots = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/annot"
path_out = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sent_rmvents/"

##%%

for j, (folder, filename) in tqdm.tqdm(enumerate(zip(["balanced","balanced","imba","imba"],["description_train","description_test","description_train","description_test"]))):
    df = pd.read_csv(f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/{folder}/{filename}.csv", header=0,index_col=0)
    # row = df.loc[0,:]
    for i, row in df.iterrows():
        # para_type = row["cat"]
        # para_filename = row.filename
        # para_filepath = f"{path_made}{para_type}/{para_filename}.txt"
        # annot_filepath = f"{path_annots}{para_type}/{para_filename}_anns.json"
        para_filename = row.sent
        para_filepath = f"{path_made}/{para_filename}.txt"
        annot_filepath = f"{path_annots}/{para_filename}_anns.json"

        with open(para_filepath, 'r', encoding='utf-8') as filehdlr:
            txt = filehdlr.read()

        with open(annot_filepath, 'r', encoding='utf-8') as filehdlr:
            annots=json.load(filehdlr)

        txt_rmv_ent = rmv_ents_by_anns(txt, annots)

        with open(f"{path_out}{para_filename}.txt", "w", encoding="utf-8") as filehdlr:
            filehdlr.write(txt_rmv_ent)

        df.loc[i,"sent_txt"] = txt_rmv_ent
    # df.to_csv(f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/{folder}/{filename}_rmvents.csv")

#%%
# lst_seg = [(int(ent['offset']), int(ent['offset'])+int(ent['length'])) for k,ent in annots.items()]
# lst_seg = sorted(lst_seg, key=lambda x:x[0])
# lst_seg = [x for sublist in lst_seg for x in sublist]
# lst_seg_ = [0] + lst_seg + [len(txt)]

# lst_pcs = [txt[x1:x2] for x1,x2 in zip(lst_seg_[0::2],lst_seg_[1::2])]
# txt_rmv_ent = "".join(lst_pcs)

# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_train.csv", header=0, index_col=0)
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_test.csv", header=0, index_col=0)
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/imba/description_train.csv", header=0, index_col=0)
df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/imba/description_test.csv", header=0, index_col=0)

df['adr'] = df['adr'].map(eval)
df['drugs'] = df['drugs'].map(eval)
df['symptoms'] = df['symptoms'].map(eval)

df_ = df.copy()
lst = []
for i, row in df.iterrows():
    para_type = row["cat"]
    para_filename = row.filename
    para_filepath = f"{path_made}{para_type}/{para_filename}.txt"
    annot_filepath = f"{path_annots}{para_type}/{para_filename}_anns.json"

    with open(para_filepath, 'r', encoding='utf-8') as filehdlr:
        txt = filehdlr.read()

    with open(annot_filepath, 'r', encoding='utf-8') as filehdlr:
        annots=json.load(filehdlr)

    txt_rmv_ent = rmv_ents_by_anns(txt, annots)
    df_.loc[i,"paragraph"] = txt_rmv_ent

# df_.to_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_train_rmvents.csv",encoding="utf-8")
# df_.to_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_test_rmvents.csv",encoding="utf-8")
# df_.to_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/imba/description_train_rmvents.csv",encoding="utf-8")
df_.to_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/imba/description_test_rmvents.csv",encoding="utf-8")

# %%
