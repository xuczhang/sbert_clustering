# -*- coding: utf-8 -*-
#%%
import numpy as np
import sys
import re
import math

import torch
from sentence_transformers import SentenceTransformer, util 
from transformers import AutoTokenizer, AutoModel
from transformers import CamembertModel, CamembertTokenizer, CamembertConfig
from transformers import FlaubertModel, FlaubertTokenizer

from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn_extra.cluster import KMedoids
from sklearn import metrics

def pooling(model_output, attention_mask, strategy='mean'):
    if strategy.lower() == 'mean':
        # Mean Pooling - Take attention mask into account for correct averaging
        token_embeddings = model_output[0] #First element of model_output contains all token embeddings
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
        return sum_embeddings / sum_mask
    elif strategy.lower() == 'cls':
        return model_output[0]
    elif strategy.lower() == 'max':
        token_embeddings = model_output[0] #First element of model_output 
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        token_embeddings[input_mask_expanded == 0] = -1e9  # Set padding tokens to large negative value
        max_over_time = torch.max(token_embeddings, 1)[0]
        return max_over_time
    else:
        raise ValueError("Unknown value for pooling strategy")

def sent_embeding_transformer(sentences, model_name, 
                                pooling_strategy='mean', 
                                return_encoded_input=False,
                                MAX_BATCH_NUM=5000):
    # update 1.1: add MAX_BATCH_NUM to seperate sentences if too long
    # 
    # SBERT Model
    # model_name = "sentence-transformers/bert-base-nli-mean-tokens"
    # model_name = "sentence-transformers/stsb-xlm-r-multilingual"
    #
    # CamemBERT:
    # |Model|	#params|	Arch.|	Training data|
    # |camembert-base|	110M|	Base|	OSCAR (138 GB of text)|
    # |camembert/camembert-large|	335M|	Large|	CCNet (135 GB of text)|
    # |camembert/camembert-base-ccnet|	110M|	Base|	CCNet (135 GB of text)|
    # |camembert/camembert-base-wikipedia-4gb|	110M|	Base|	Wikipedia (4 GB of text)|
    # |camembert/camembert-base-oscar-4gb|	110M|	Base|	Subsample of OSCAR (4 GB of text)|
    # |camembert/camembert-base-ccnet-4gb|	110M|	Base|	Subsample of CCNet (4 GB of text)|
    #
    # FlauBERT models
    # |Model name|	Number of layers|	Attention Heads|	Embedding Dimension|	Total Parameters|
    # |flaubert/flaubert-small-cased|	6|	8|	512|	54 M|
    # |flaubert/flaubert-base-uncased|	12|	12|	768|	137 M|
    # |flaubert/flaubert-base-cased|	12|	12|	768|	138 M|
    # |flaubert/flaubert-large-cased|	24|	16|	1024|	373 M|
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModel.from_pretrained(model_name)

    if len(sentences) > MAX_BATCH_NUM:
        division  = math.ceil(len(sentences)/MAX_BATCH_NUM)
        sentences_split = [sentences[(MAX_BATCH_NUM * i):(MAX_BATCH_NUM * (i + 1))] for i in range(division)]

        lst_encoded_input = [tokenizer(sents, padding=True, truncation=True, max_length=128, return_tensors='pt') for sents in sentences_split]

        lst_model_output = []
        lst_sent_embd = []
        for encoded_input in lst_encoded_input:
            with torch.no_grad():
                model_output = model(**encoded_input)
            lst_model_output.append(model_output)
            lst_sent_embd.append(pooling(model_output, encoded_input['attention_mask'], strategy=pooling_strategy))

        sentence_embeddings = torch.cat(lst_sent_embd)
    else:
        #Tokenize sentences
        encoded_input = tokenizer(sentences, padding=True, truncation=True, max_length=128, return_tensors='pt')

        #Compute token embeddings
        with torch.no_grad():
            model_output = model(**encoded_input)

        #Perform pooling, mean pooling by default
        sentence_embeddings = pooling(model_output, encoded_input['attention_mask'], strategy=pooling_strategy)
        if return_encoded_input:
            return sentence_embeddings, encoded_input
    return sentence_embeddings

def sent_embeding_sbert(sentences, 
                        model_name='sentence-transformers/bert-base-nli-mean-tokens'):
    model = SentenceTransformer(model_name)
    sentence_embeddings = model.encode(sentences)
    return sentence_embeddings

# ------------------------ Clustering ----------------------------------
def cluster_KMeans(corpus, embeddings, num_clusters=5):
    clustering_model = KMeans(n_clusters=num_clusters)
    clustering_model.fit(embeddings)
    labels = clustering_model.predict(embeddings)
    centroids  = clustering_model.cluster_centers_
    cluster_assignment = clustering_model.labels_
    centroid_labels = [centroids[i] for i in labels]
    
    #print("%.6f" % metrics.v_measure_score(centroid_labels, clustering_model.labels_))

    clustered_sentences = [[] for i in range(num_clusters)]
    for sentence_id, cluster_id in enumerate(cluster_assignment):
        clustered_sentences[cluster_id].append(corpus[sentence_id])
   
    return clustered_sentences
   
def cluster_HAC(corpus, embeddings, num_clusters = 5, affinity='euclidean', linkage='ward', return_labels=False):
    '''
    # Possible metric used to compute the linkage: "euclidean", "l1", "l2", "manhattan", "cosine", or "precomputed"
    #
    # linkage{‘ward’, ‘complete’, ‘average’, ‘single’}, default=’ward’
    # Which linkage criterion to use. The linkage criterion determines which distance to use between sets of observation. The algorithm will merge the pairs of cluster that minimize this criterion.
    #
    # ‘ward’ minimizes the variance of the clusters being merged.
    # ‘average’ uses the average of the distances of each observation of the two sets.
    # ‘complete’ or ‘maximum’ linkage uses the maximum distances between all observations of the two sets.
    # ‘single’ uses the minimum of the distances between all observations of the two sets.
    '''
    cluster = AgglomerativeClustering(num_clusters, affinity=affinity, linkage=linkage)
    cluster.fit_predict(embeddings)
  
    cluster_assignment = cluster.labels_
    
    clustered_sentences = [[] for i in range(num_clusters)]
    for sentence_id,cluster_id in enumerate(cluster_assignment):
        clustered_sentences[cluster_id].append(corpus[sentence_id])
    if return_labels:
        return clustered_sentences, cluster_assignment
    return clustered_sentences


def cluster_kMedoids(corpus, embeddings, num_clusters = 5, affinity='euclidean', return_labels=False):
    '''https://scikit-learn-extra.readthedocs.io/en/latest/generated/sklearn_extra.cluster.KMedoids.html'''
    kmedoids = KMedoids(n_clusters=num_clusters, metric=affinity).fit(embeddings)
    #     Attributes
    # cluster_centers_ : array, shape = (n_clusters, n_features) or None if metric == ‘precomputed’. Cluster centers, i.e. medoids (elements from the original dataset)
    #
    # medoid_indices_: array, shape = (n_clusters,) The indices of the medoid rows in X
    #
    # labels_ : array, shape = (n_samples,) Labels of each point
    #
    # inertia_ : float Sum of distances of samples to their closest cluster center.
    cluster_assignment = kmedoids.labels_
    
    clustered_sentences = [[] for i in range(num_clusters)]
    for sentence_id,cluster_id in enumerate(cluster_assignment):
        clustered_sentences[cluster_id].append(corpus[sentence_id])
    if return_labels:
        return clustered_sentences, cluster_assignment
    return clustered_sentences

# ------------------------ Similarity in URE-SBERT (Modified)----------------------------------
def similarity(cluster,queries,cent, 
                k=7, threshold = 0.9,
                model_name='sentence-transformers/bert-base-nli-mean-tokens',pooling_strategy="mean"):
    patterns = {}
    paterns_pharses = []
    # embedder = SentenceTransformer(model_name)
    # corpus_embeddings = embedder.encode(cluster, convert_to_tensor=True)
    corpus_embeddings = sent_embeding_transformer(cluster, model_name, pooling_strategy=pooling_strategy)
    if cent:
        for query in queries:
            # query_embedding = embedder.encode(query, convert_to_tensor=True)
            query_embedding = sent_embeding_transformer(cluster, model_name, pooling_strategy=pooling_strategy)
            cos_scores = util.pytorch_cos_sim(query_embedding, corpus_embeddings)[0]
            cos_scores = cos_scores.cpu()
            top_results = np.argpartition(-cos_scores, range(k))[0:k]
            for idx in top_results[0:k]:
                if cos_scores[idx] > threshold:
                    t = re.findall(r"[-+]?\d*\.\d+|\d+",str(cos_scores[idx]) )
                    score = float(t[0])
                    patterns[cluster[idx].strip()] = score
                    paterns_pharses.append(cluster[idx].strip())
    else:
        for query, value in queries.items():
            query_embedding = sent_embeding_transformer(cluster, model_name, pooling_strategy=pooling_strategy)
            cos_scores = util.pytorch_cos_sim(query_embedding, corpus_embeddings)[0]
            cos_scores = cos_scores.cpu()
            top_results = np.argpartition(-cos_scores, range(k))[0:k]
            for idx in top_results[0:k]:
                if cos_scores[idx] > threshold:
                    t = re.findall(r"[-+]?\d*\.\d+|\d+",str(cos_scores[idx]) )
                    score = float(t[0])        
                    patterns[cluster[idx].strip()] = score - (1 - value)
                    paterns_pharses.append(cluster[idx].strip())
    return patterns, paterns_pharses

    
def similarity_extract(pat,sent,k=1, threshold = 0.5,
                        model_name='sentence-transformers/bert-base-nli-mean-tokens',pooling_strategy="mean"):
    find = False
    # embedder = SentenceTransformer('distilbert-base-nli-stsb-mean-tokens')
    # pat_embed = embedder.encode(pat, convert_to_tensor=True)
    # sent_embed= embedder.encode(sent,convert_to_tensor=True)
    pat_embed = sent_embeding_transformer(pat, model_name, pooling_strategy=pooling_strategy)
    sent_embed= sent_embeding_transformer(sent, model_name, pooling_strategy=pooling_strategy)
        
    cos_scores = util.pytorch_cos_sim(pat_embed, sent_embed)[0]
    cos_scores = cos_scores.cpu()
    top_results = np.argpartition(-cos_scores, range(k))[0:k]
    for idx in top_results[0:k]:
        print(cos_scores[idx])
        if cos_scores[idx] > threshold:
                find = True

    return find



#%%
if __name__ == "__main__":
    sentence = "Réticulations sous-pleurales avec aspect en rayon de miel intéressant le lobe moyen, les deux lobes inférieurs et la lingula,  évocateur de fibrose pulmonaire."

    modelname = "camembert-base"
    sentence_embeddings1 = sent_embeding_transformer(sentence, modelname, pooling_strategy='mean')
    print("1st embedding complete")

    
# %%
