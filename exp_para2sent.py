#%%
import sys
import pandas as pd
import re as re_
import json
import pathlib
import ure_sbert

from pprint import pprint

from nltk.tokenize import sent_tokenize
# import nltk.data
# sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')

import tqdm

#%%

para_location = 'C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/par'

df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/description.csv",sep=';')
df['adr'] = df['adr'].map(eval)
df['drugs'] = df['drugs'].map(eval)
df['symptoms'] = df['symptoms'].map(eval)


df_ = df[(df.cat==1)|(df.cat==2)]
df_ = df_[['doc', 'filename', 'paragraph', 'cat']]
df_.columns = ['doc', 'paragraph', 'para_txt', 'para_cat']
df_.index=range(len(df_))

# %% Find all sentences
df_sent = pd.DataFrame()
for i,row in tqdm.tqdm(df_.iterrows()):
    txt = row["para_txt"]
    lst_s = []
    lst_e = []
    lst_tmp_sents = sent_tokenize(txt,language='english')
    if not lst_tmp_sents:
        continue
    if len(lst_tmp_sents)==1:
        df_tmp = pd.Series({'doc':row['doc'], 'paragraph':row['paragraph'], 'para_txt':row['para_txt'], 'para_cat':row['para_cat'],'sent':f"{row.paragraph}_sent1",'sent_start':0,'sent_end':len(lst_tmp_sents[0]),'sent_txt':lst_tmp_sents[0]})
    else:
        for t in lst_tmp_sents:
            if not re_.search('[a-zA-Z]',t):
                # no character at all
                # print(f"\'{i}\'-1: {t}")
                continue
            if re_.match('^\s*[0-9]+[.\/)] ?\W?',t) and not re_.search('[a-zA-Z]',t):
                # ignore digits begin
                # print(f"\'{i}\'-2: {t}")
                continue
            if re_.match('^\s*([0-9] )?[0-9]+[.\/)] ?\W?',t) and not re_.search('[a-zA-Z]',t):
                # "2 2." for example
                # print(f"\'{i}\'-3: {t}")
                continue
            if re_.match('^\s*[0-9]+[a-zA-z][.\/)] ?\W?',t):
                #  "\n 1A. " for example
                # print(f"\'{i}\'-4: {t}")
                continue
            if re_.match('^\s*[a-zA-z][0-9]?[.\/)] ?\W?',t):
                # "\n A. ", "\n a) " for example
                # print(f"\'{i}\'-5: {t}")
                continue
            if re_.match('^\s*[a-z]+',t) and len(t)<=45:
                # if begin with lower case, its a part of unfinished sentence
                # print(f"\'{i}\'-6: {t}")
                if lst_e:
                    lst_e[-1] += len(t)
                    continue
            s,e = txt.index(t),txt.index(t)+len(t)
            lst_s.append(s)
            lst_e.append(e)
        lst_sent_txt = [txt[s:e] for s,e in zip(lst_s,lst_e)]
        df_tmp = pd.DataFrame({'doc':[row['doc']]*len(lst_s), 'paragraph':[row['paragraph']]*len(lst_s), 'para_txt':[row['para_txt']]*len(lst_s), 'para_cat':[row['para_cat']]*len(lst_s),'sent':[f"{row.paragraph}_sent{i+1}" for i in range(len(lst_s))],'sent_start':lst_s,'sent_end':lst_e,'sent_txt':lst_sent_txt})
    df_sent = df_sent.append(df_tmp,ignore_index=True)
df_sent['para_cat'] = df_sent['para_cat'].astype(int)

df_tmp = df_sent[df_sent.sent_txt.map(len)<=5]

##%% split para_annot file into sents
SAVE_RESULT = True
para_annot_path = 'C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/MADE_para_ents/'
pathout = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sent"
pathout_ann = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/annot"

# annot file
for i, row in tqdm.tqdm(df_sent.iterrows()):
    sent = row.sent
    cat = int(row['para_cat'])
    with open(para_annot_path+f'{cat}/{row.paragraph}_anns.json','r') as filehdl:
        annots = json.load(filehdl)

    df_tmp  = df_sent[df_sent.paragraph==row.paragraph]
    if len(df_tmp)<2:
        # print(f"only 1 sent: {sent}")
        if SAVE_RESULT:
            with open(f"{pathout_ann}/{sent}_anns.json", 'w') as filehdl:
                # print(f'->Saving: {filehdl.name}')
                json.dump(annots, filehdl, indent=4, ensure_ascii=False)
        continue
    
    for j,asent in df_tmp.iterrows(): 
        start = int(asent.sent_start)
        end = int(asent.sent_end) 
        dict_row={}
        for key,ent in annots.items():
            if start <= int(ent['offset']) <= end:
                tmp = ent.copy()
                tmp['offset'] -= start
                dict_row[key] = tmp
        # print("J:",j,end=' ')
        # pprint(dict_row)
        if SAVE_RESULT:
            with open(f"{pathout_ann}/{asent.sent}_anns.json", 'w') as filehdl:
                # print(f'->Saving: {filehdl.name}')
                json.dump(dict_row, filehdl, indent=4, ensure_ascii=False)
    # print(f"I:{i}",end=' ')
    # pprint(annots)

# sentence cat
lst_cat = []
for i, row in df_sent.iterrows():
    with open(pathout_ann+f'/{row.sent}_anns.json','r') as filehdl:
        annots = json.load(filehdl)
    set_labels = set([v['label'] for k,v in annots.items()])
    if len(set_labels)==0:
        lst_cat.append(0)
    elif 'ADE' in set_labels:
        if "DRUG" not in set_labels:
            lst_cat.append(3)
        else:
            lst_cat.append(2)
    else:
        lst_cat.append(1)
df_sent['sent_cat'] = lst_cat
print(df_sent.sent_cat.value_counts())
# df & txt file
if SAVE_RESULT:
    df_sent.to_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sentences_descriptions.csv")

    for i, row in df_sent.iterrows():
        with open(f"{pathout}/{row.sent}.txt", 'w') as filehdl:
            # print(f'->Saving: {filehdl.name}')
            filehdl.write(row.sent_txt)

## %% sampled dataframe of paragraphs => split into sents 


# df_sent = df_sent.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sentences_descriptions.csv",header=0,index_col=0)

for i, (folder, data_type) in enumerate(zip(["balanced","balanced","imba","imba"],["description_train","description_test","description_train","description_test"])):
    # if i==1:
    #     break
    print(f"{folder}/{data_type}")
    df_par = pd.read_csv(f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/{folder}/{data_type}.csv",header=0,index_col=0)
    lst_sampled_paras = df_par.filename.to_list()
    df_sent_sampled = df_sent[df_sent.paragraph.isin(lst_sampled_paras)]
    df_sent_sampled.to_csv(f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/{folder}/{data_type}.csv")
    print("{:=<30}".format(""))
    print(len(df_par),len(df_sent_sampled.paragraph.unique()))


# %% Remove entities

def rmv_ents_by_anns(txt:str, annots:dict):
    if not annots:
        return txt
    # Each offset is the endpoint of a segment, the end of each entity is the startpoint of a segment
    lst_seg = [(int(ent['offset']), int(ent['offset'])+int(ent['length'])) for k,ent in annots.items()]
    lst_seg = sorted(lst_seg, key=lambda x:x[0])
    lst_seg = [x for sublist in lst_seg for x in sublist]
    lst_seg_ = [0] + lst_seg + [len(txt)]

    lst_pcs = [txt[x1:x2] for x1,x2 in zip(lst_seg_[0::2],lst_seg_[1::2])]
    txt_rmv_ent = "".join(lst_pcs)
    return txt_rmv_ent

path_out_rmvents = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_sentences/sent_rmvents/"

for j, (folder, filename) in tqdm.tqdm(enumerate(zip(["balanced","balanced","imba","imba"],["description_train","description_test","description_train","description_test"]))):
    df = pd.read_csv(f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/{folder}/{filename}.csv", header=0,index_col=0)
    # row = df.loc[0,:]
    for i, row in df.iterrows():
        # para_type = row["cat"]
        # para_filename = row.filename
        # para_filepath = f"{path_made}{para_type}/{para_filename}.txt"
        # annot_filepath = f"{path_annots}{para_type}/{para_filename}_anns.json"
        para_filename = row.sent
        para_filepath = f"{pathout}/{para_filename}.txt"
        annot_filepath = f"{pathout_ann}/{para_filename}_anns.json"

        with open(para_filepath, 'r', encoding='utf-8') as filehdlr:
            txt = filehdlr.read()

        with open(annot_filepath, 'r', encoding='utf-8') as filehdlr:
            annots=json.load(filehdlr)

        txt_rmv_ent = rmv_ents_by_anns(txt, annots)

        with open(f"{path_out_rmvents}{para_filename}.txt", "w", encoding="utf-8") as filehdlr:
            filehdlr.write(txt_rmv_ent)

        df.loc[i,"sent_txt"] = txt_rmv_ent
    df.to_csv(f"C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents/{folder}/{filename}_rmvents.csv")

# %%
