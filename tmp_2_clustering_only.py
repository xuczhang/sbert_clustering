# -*- coding: utf-8 -*-
#%%
import sys
import pandas as pd
import re as re_
import json
import pathlib

import sentEmbd_clsuter as sc_
import ure_sbert

from sklearn import metrics

from pprint import pprint

import gap_statistic as gap_stats

#%% DIFF pre_type & data_type
exp_type = "para"  #"para", "sent_cat0123" 
embd_method = 'biobert'

C_gapstats = False
num_gapstats = 1 # 1,2,3
num_clusters = 2

affinity='euclidean' # cosine, euclidean
lst_affinity = ["cosine",'euclidean']
# lst_affinity = ['euclidean']

linkage_HAC = "average"

path_df_base = "C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled_sents"
path_embd_base = "C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/embedding/"
path_cltr_base = "C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/clustering/"
path_out = "C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/clustering/assignments"

lst_pretype = ['min','spwd','lemma','spwd_lemma']
# # para data
lst_data_type = ['bala_train_data','bala_train_data_rmvents','imba_train_data','imba_train_data_rmvents']
lst_descrip_file = ['balanced/description_train.csv', 'balanced/description_train_rmvents.csv', 'imba/description_train.csv', 'imba/description_train_rmvents.csv']
# sents data
# lst_descrip_file = ['balanced/description_train.csv', 'balanced/description_train_rmvents.csv', 'imba/description_train.csv', 'imba/description_train_rmvents.csv']
# lst_data_type = ['bala_train_sents_cat0123','bala_train_sents_cat0123_rmvents','imba_train_sents_cat0123','imba_train_sents_cat0123_rmvents']

if C_gapstats:
    for i in range(3):
        ##%% predefine num_clusters for each exp by gapstats, if needed
        ## the gapstats params will be stored in `path_embd_base` folder
        params_filename = f"{exp_type}_exps_clustering_params.csv"
        params_filename = f"{exp_type}_exps_clustering_gapstats_params_{i+1}.csv"
        if pathlib.Path(f"{path_cltr_base}/{params_filename}").exists():
            df_params = pd.read_csv(f"{path_cltr_base}/{params_filename}", header=0, index_col=0)
            lst_num_cluster = df_params.num_clusters.astype(int).to_list()
        else:
            df_params = pd.DataFrame()
            if C_gapstats:
                gap_model=gap_stats.OptimalK(parallel_backend='rust')
                
            lst_num_cluster = []
            for des_file, data_type in zip(lst_descrip_file,lst_data_type):
                for pre_type in lst_pretype:
                    path_df_file = f"{path_df_base}/{des_file}"
                    path_embd = f"{path_embd_base}/{data_type}/sbert_mean_{pre_type}/"
                    if C_gapstats:
                        encoding = sc_.torch.load(path_embd+"encoding.pt")
                        num_clusters = gap_model(encoding.detach().cpu().numpy(),cluster_array=range(1,11))    
                    lst_num_cluster.append(num_clusters)

                    df_params = df_params.append(pd.Series({"data":data_type,'embedding':embd_method,'preprocessing':pre_type,'num_clusters':num_clusters}), ignore_index=True)
                    print(data_type,embd_method,pre_type,num_clusters)

            df_params.to_csv(f"{path_cltr_base}/{params_filename}")
else:
    lst_num_cluster = [num_clusters]*(len(lst_data_type)*len(lst_pretype))

#%% Clustering, eval & assignments, into DataFrame and save
if "sent" in exp_type :
    filename_col = "sent" 
    cat_col = "sent_cat" 
else:
    # para level
    filename_col = "paragraph"#'filename'
    cat_col = "para_cat"#'cat'

bert_type = "sbert_mean" if "sbert" in embd_method else 'biobert_mean'

if cat_col == 'sent_cat':
    cat_type = "senLvlCat" 
elif  cat_col == 'cat':
    cat_type = "parLvlCat"
elif cat_col == 'doc_cat':
    cat_type = "docLvlCat"
else:
    cat_type = "unknownCat"

df_res = pd.DataFrame()
# df_cls = pd.DataFrame(df.sent)

for i, (des_file, data_type) in enumerate(zip(lst_descrip_file,lst_data_type)):
    for j, pre_type in enumerate(lst_pretype):
        num_clusters = lst_num_cluster[4*i+j]
        path_df_file = f"{path_df_base}/{des_file}"
        path_embd = f"{path_embd_base}/{data_type}/{bert_type}_{pre_type}/"

        df = pd.read_csv(path_df_file, header=0, index_col=0)
        if i==0 and j==0:
            if "sent" in exp_type :
                df_cls = pd.DataFrame(df.sent)
            else:
                df_cls = pd.DataFrame(df.paragraph.unique())

        dict_filename_cat = df[[filename_col,cat_col]].set_index(filename_col).to_dict()[cat_col]
        lst_cat4sent = df[cat_col].to_list()

        with open(path_embd+"selected_files.txt",'r') as filehdl:
            lst_files_all = filehdl.readlines()
        lst_files_all = [x.strip("\n") for x in lst_files_all]

        with open(path_embd+"selected_sentences.txt",'r') as filehdl:
            selected_sentences = filehdl.readlines()

        sentence_dict = dict(zip(lst_files_all,selected_sentences))

        encoding = sc_.torch.load(path_embd+"encoding.pt")

        for affinity in lst_affinity:
            print(data_type,embd_method,pre_type,num_clusters,affinity)
            # HAC
            # res = read_and_eval(path_df_file, path_embd, num_clusters=num_clusters, cls_func='HAC', affinity=affinity, linkage_HAC = linkage_HAC)
            # lst_eval_methods = list(res.keys())
            clustered_sentences, cluster_assignment = sc_.cluster_HAC(selected_sentences, encoding ,num_clusters, linkage=linkage_HAC, affinity=affinity, return_labels=True) 
            # cluster_assignment +=1
            # res = evaluate_clustering(lst_cat4sent, cluster_assignment, encoding, affinity)
            # lst_eval_methods = list(res.keys())
            # res['data_type'] = data_type
            # res['embd_method'] = embd_method
            # res['preprocess_type'] = pre_type
            # res['clustering'] = 'HAC'
            # res['metric']=affinity
            # res['num_clusters'] = num_clusters
            # res['cat_level'] = cat_type
            # df_res = df_res.append(res, ignore_index=True)
            onecase = f"{data_type}_{embd_method}_{pre_type}_HAC_{affinity}_cls"
            if len(df_cls) != len(cluster_assignment):
                tmp_filename = lst_data_type[i-1]
                if "bala" in tmp_filename:
                    tmp_filename = f"bala_train_{exp_type}"
                else:
                    tmp_filename = f"imba_train_{exp_type}"
                if C_gapstats:
                    df_cls.to_csv(path_out+f"/{tmp_filename}_{embd_method}_gapstat_clusters_assignment.csv")
                else:
                    df_cls.to_csv(path_out+f"/{tmp_filename}_{embd_method}_{num_clusters}clusters_assignment.csv")
                if "sent" in exp_type :
                    df_cls = pd.DataFrame(df.sent)
                else:
                    df_cls = pd.DataFrame(df.paragraph.unique())
                df_cls[onecase] = cluster_assignment
            else:
                df_cls[onecase] = cluster_assignment

            # Kmedoids
            # res = read_and_eval(path_df_file, path_embd, num_clusters=num_clusters, cls_func="kMedoids", affinity=affinity)
            clustered_sentences, cluster_assignment = sc_.cluster_kMedoids(selected_sentences, encoding ,num_clusters, affinity=affinity, return_labels=True) 
            # cluster_assignment +=1
            # res = evaluate_clustering(lst_cat4sent, cluster_assignment, encoding, affinity)
            # res['data_type'] = data_type
            # res['embd_method'] = embd_method
            # res['preprocess_type'] = pre_type
            # res['clustering'] = 'kMedoids'
            # res['metric']=affinity
            # res['num_clusters'] = num_clusters
            # res['cat_level'] = cat_type
            # df_res = df_res.append(res, ignore_index=True)
            onecase = f"{data_type}_{embd_method}_{pre_type}_KMedoids_{affinity}_cls"
            df_cls[onecase] = cluster_assignment

# df_ = df_res[['data_type','embd_method','preprocess_type','clustering','metric','num_clusters']+lst_eval_methods]

# # Optional, for previewing
# if "8_Contingency Matrix" in df_.columns:
#     df_["8_Contingency Matrix"] = df_["8_Contingency Matrix"].map(lambda txt:str(txt).replace('\n',','))
# if '9_pair_confusion_matrix' in df_.columns:
#     df_['9_pair_confusion_matrix'] = df_['9_pair_confusion_matrix'].map(lambda txt:str(txt).replace('\n',','))

tmp_filename = lst_data_type[i-1]
if "bala" in tmp_filename:
    tmp_filename = f"bala_train_{exp_type}"
else:
    tmp_filename = f"imba_train_{exp_type}"
if C_gapstats:
    # df_.to_csv(path_out+f"/{exp_type}_{cat_type}_{affinity}_gapstat_clusters_eval.csv")
    df_cls.to_csv(path_out+f"/{tmp_filename}_{embd_method}_gapstat_clusters_assignment.csv")
else:
    # df_.to_csv(path_out+f"/{exp_type}_{cat_type}_{affinity}_{num_clusters}clusters_eval.csv")
    df_cls.to_csv(path_out+f"/{tmp_filename}_{embd_method}_{num_clusters}clusters_assignment.csv")

print("Finished")

# %%
