# -*- coding: utf-8 -*-
#%%
import sys
import pandas as pd
import re as re_
import json
import pathlib

import sentEmbd_clsuter as sc_
import ure_sbert
BAD_TOKENS = [",", "(", ")", ";", "''",  "``", "'s", "-", "vs.", "v", "'", ":", ".", "--"]
# STOP_WORDS = set(stopwords.words('english'))

#%% ===================== MADE ===============================================
# Original MADE
# df = pd.read_csv("../data/corrected_MADE_data_splitted_by_paragraphs/description.csv",sep=';')
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/Topix_LTBM/Exps/MADE_ba/seed_218/processed_data/data_preprocessed_spwd.csv",header=0,index_col=0)
# df['adr'] = df['adr'].map(eval)
# df['drug_names'] = df['drug_names'].map(eval)
# df['symp_names'] = df['symp_names'].map(eval)

# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/description_train.csv",sep=';', header=0,index_col=0)
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_splitted_by_paragraphs/description.csv",sep=';', header=0,index_col=0)

# bala
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_train.csv", header=0,index_col=0)
# df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/balanced/description_train_rmvents.csv", header=0,index_col=0)

# imba
df = pd.read_csv("C:/Users/Xuchun/boulot/Work/data/corrected_MADE_data_sampled/imba/description_train_rmvents.csv", header=0,index_col=0)

df['adr'] = df['adr'].map(eval)
df['drugs'] = df['drugs'].map(eval)
df['symptoms'] = df['symptoms'].map(eval)

#%% Init
# MADE_location="../data/corrected_MADE_data/"
# para_location = '../data/corrected_MADE_data_splitted_by_paragraphs/par'

SAVE_INTERMEDIATE = True

# pre_type = "min"
# pre_type = "spwd"
# pre_type = "lemma"
pre_type = "spwd_lemma"
model_name = "sentence-transformers/bert-base-nli-mean-tokens"
# model_name = "dmis-lab/biobert-base-cased-v1.1"
pooling_strategy = "mean"
# Possible metric used to compute the linkage: "euclidean", "l1", "l2", "manhattan", "cosine", or "precomputed"

exp_num = F"sbert_{pooling_strategy}_{pre_type}"
path_out = f"C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/imba_train_data_rmvents/{exp_num}/"
# path_out = f"C:/Users/Xuchun/boulot/Work/Exps/SBERT/MADE/balance_train_data_rmvents/{exp_num}/"

if not pathlib.Path(path_out).exists():
    pathlib.Path(path_out).mkdir(parents=True, exist_ok=True)

print(f"->OUT DIR: {path_out}")
print(f"->Preprocessing type: {pre_type}")
print(f"->MODEL: {model_name}")

with open(path_out+"params.txt", 'w') as filehdl:
    print(f"=> Saving: {filehdl.name}")
    filehdl.write(f"Raw text\n")
    filehdl.write(f"Preprocessing: {pre_type}\n")
    filehdl.write(f"Pooling strategy: {pooling_strategy}\n")
    # filehdl.write(f"{cluster_method} cluster, metric: {affinity}\n")
    filehdl.write(f"model_name: {model_name}\n")

##%% Sentences Selection
dict_filename_cat = df[['filename','cat']].set_index('filename').to_dict()['cat']

# All
df_tmp = df[['filename','paragraph']].set_index('filename')

# # all pos + neg
# df_ = df[(df.cat==1)|(df.cat==2)]
# df_tmp = df_[['filename','paragraph']].set_index('filename')

# N samples
# rng_seed = 20
# n_samples = 20
# df_tmp = pd.concat([df[df.cat==1].sample(n_samples, random_state=rng_seed), df[df.cat==2].sample(n_samples, random_state=rng_seed)])[['filename','paragraph']].set_index('filename')

##%% Preprocessing
df_pre = ure_sbert.apply_one_preprocess(df_tmp, pre_type, target_col='paragraph')
sentence_dict = df_pre.to_dict()['paragraph']
selected_sentences = list(sentence_dict.values())

if SAVE_INTERMEDIATE:
    with open(path_out+"selected_files.txt",'w') as filehdl:
        print(f"=> Saving: {filehdl.name}")
        filehdl.writelines('\n'.join(sentence_dict.keys()))

    with open(path_out+"selected_sentences.txt",'w') as filehdl:
        print(f"=> Saving: {filehdl.name}")
        filehdl.writelines('\n'.join(sentence_dict.values()))

# %% Embedding
encoding = sc_.sent_embeding_transformer(selected_sentences, model_name, pooling_strategy=pooling_strategy)

if SAVE_INTERMEDIATE:
    print(f"=> Saving: {path_out + 'encoding.pt'}")
    sc_.torch.save(encoding, path_out + 'encoding.pt')

#%% Clustering

cluster_method = "HAC"
# cluster_method = "kmethoids"
affinity = "euclidean"

outdir = f"{path_out}{cluster_method}/"
if not pathlib.Path(outdir).exists():
    pathlib.Path(outdir).mkdir()

with open(outdir+f"exp_{exp_num}_log.txt",'w') as filehdl:
    print(f"=> Saving: {filehdl.name}")
    filehdl.write(f"{len(df)} paragraphs selected.")
    filehdl.write(f"num pos: {len(df[df.cat==2])}, num neg: {len(df[df.cat==1])}\n")

lst_num_cluster = list(range(2,11))
# exp_num = 2
lst_each_case = []
for num_cluster in lst_num_cluster:

    if cluster_method == "HAC":
        clustered_sentences = sc_.cluster_HAC(selected_sentences, encoding ,int(num_cluster), affinity=affinity) 
    elif cluster_method == "kmethoids":
        clustered_sentences = sc_.cluster_kMedoids(selected_sentences, encoding ,int(num_cluster), affinity=affinity) 

    lst_cls = []
    for l in clustered_sentences:
        lst_cls.append([ure_sbert.find_key(sentence_dict, x) for x in l])

    lst_cls_cat = [[dict_filename_cat[x] for x in l] for l in lst_cls]
    df_cls = pd.DataFrame(lst_cls_cat).T

    lst_each_case.append([f"{num_cluster}",f"cat1"] + [sum([x for x in l if x==1]) for l in lst_cls_cat])
    lst_each_case.append([f"{num_cluster}",f"cat2"] + [sum([x for x in l if x==2]) for l in lst_cls_cat])

    log = []
    for i in range(num_cluster):
        log.append("{:=<30}".format(''))
        log.append(f"cluster {i}")
        log.append(str(df_cls.loc[:,i].value_counts()).replace('\n','\r\n'))
        log.append("{:=<30}".format(''))
    log_str = '\r\n'.join(log)
    # print(log_str)

    with open(outdir+f"exp_{exp_num}_{num_cluster}clusters.txt",'w') as filehdl:
        print(f"=> Saving: {filehdl.name}")
        filehdl.writelines(["\n"+str(x) for x in lst_cls])

    with open(outdir+f"exp_{exp_num}_log.txt",'a') as filehdl:
        filehdl.write(f"\n\nExp: {exp_num}")
        filehdl.write(f"\nCluster Method: HAC, affinity: {affinity}")
        filehdl.write(f"\nNum cluster: {num_cluster}\n")
        filehdl.writelines(log_str)

df_tmp = pd.DataFrame(lst_each_case)
df_tmp.columns = ["num_clusters",'cat']+[f"nb_cls{x}" for x in range(1,11)]
df_tmp.to_csv(outdir+f"exp_{exp_num}_cluster_cat_count.csv")
print(f"=> Saving: {outdir}"+f"exp_{exp_num}_cluster_cat_count.csv")

# # %% Similarity
# # similiraty: 0.1
# # top_similar: 15
# # query_term: born in
# top_k = 15
# smlty = 0.1
# query_term = ""

# %% Clustering 2

cluster_method = "kmethoids"
affinity = "euclidean"

outdir = f"{path_out}{cluster_method}/"
if not pathlib.Path(outdir).exists():
    pathlib.Path(outdir).mkdir()

with open(outdir+f"exp_{exp_num}_log.txt",'w') as filehdl:
    print(f"=> Saving: {filehdl.name}")
    filehdl.write(f"{len(df)} paragraphs selected.")
    filehdl.write(f"num pos: {len(df[df.cat==2])}, num neg: {len(df[df.cat==1])}\n")

lst_num_cluster = list(range(2,11))
# exp_num = 2
lst_each_case = []
for num_cluster in lst_num_cluster:

    if cluster_method == "HAC":
        clustered_sentences = sc_.cluster_HAC(selected_sentences, encoding ,int(num_cluster), affinity=affinity) 
    elif cluster_method == "kmethoids":
        clustered_sentences = sc_.cluster_kMedoids(selected_sentences, encoding ,int(num_cluster), affinity=affinity) 

    lst_cls = []
    for l in clustered_sentences:
        lst_cls.append([ure_sbert.find_key(sentence_dict, x) for x in l])

    lst_cls_cat = [[dict_filename_cat[x] for x in l] for l in lst_cls]
    df_cls = pd.DataFrame(lst_cls_cat).T

    lst_each_case.append([f"{num_cluster}",f"cat1"] + [sum([x for x in l if x==1]) for l in lst_cls_cat])
    lst_each_case.append([f"{num_cluster}",f"cat2"] + [sum([x for x in l if x==2]) for l in lst_cls_cat])

    log = []
    for i in range(num_cluster):
        log.append("{:=<30}".format(''))
        log.append(f"cluster {i}")
        log.append(str(df_cls.loc[:,i].value_counts()).replace('\n','\r\n'))
        log.append("{:=<30}".format(''))
    log_str = '\r\n'.join(log)
    # print(log_str)

    with open(outdir+f"exp_{exp_num}_{num_cluster}clusters.txt",'w') as filehdl:
        print(f"=> Saving: {filehdl.name}")
        filehdl.writelines(["\n"+str(x) for x in lst_cls])

    with open(outdir+f"exp_{exp_num}_log.txt",'a') as filehdl:
        filehdl.write(f"\n\nExp: {exp_num}")
        filehdl.write(f"\nCluster Method: HAC, affinity: {affinity}")
        filehdl.write(f"\nNum cluster: {num_cluster}\n")
        filehdl.writelines(log_str)

df_tmp = pd.DataFrame(lst_each_case)
df_tmp.columns = ["num_clusters",'cat']+[f"nb_cls{x}" for x in range(1,11)]
df_tmp.to_csv(outdir+f"exp_{exp_num}_cluster_cat_count.csv")
print(f"=> Saving: {outdir}"+f"exp_{exp_num}_cluster_cat_count.csv")
# %%
